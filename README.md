# Surprise du 16 mai 2018 pour l'UV SR03

## Consignes initiales

Il y a normalement 4 fichiers et un dossier contenant un cinquième fichier.
Il faut télécharger l'ensemble des fichiers.
Les éditer avec un éditeur type Sublime Text, Notepad ++ ou Atom.
Utiliser le navigateur Firefox afin de ne pas avoir de problème de "cross domains".

## Les consignes suivantes sont dans le fichier Consignes.pdf