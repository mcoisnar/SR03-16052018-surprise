// Variable globale pour stocker la réponse de la XHR
var leschats;

function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}

function DOMcorrection() {
    // Récupérer le paragraphe avec l'id version 
    const version = document.getElementById('version'); 
    // Corriger le numéro de la version
    version.innerHTML = "Version 1.0";


    // Créer un attribut style
    var monstyle = document.createAttribute("style");
    // Ajout de la propriété "italique" pour la police
    monstyle.value = "font-style: italic;";
    // Récupérer le paragraphe pour lequel on souhaite appliquer le style
    var cible = document.getElementById('Resume');
    // Ajouter l'attribut style au paragraphe
    cible.setAttributeNode(monstyle);


    // Récupérer la balise span avec l'id "dump"
    const dump = document.getElementById("dump");
    // Changer son contenu texte
    dump.innerHTML = "intelligent";
    // Mettre la police en gras
    dump.style.fontWeight = "bold";


    // Récupérer une collection de paragraphes
    var paragraphs = document.getElementsByTagName("p");
    // Parcourir tous les paragraphes
    for (let i = 0; i < paragraphs.length; i++) {
        // Pour chaque paragraphe changer la couleur de la police en gris
        paragraphs[i].style.color = "grey";
    }
}


function allcat() {
    // Requête XHR pour récupérer la liste des chats

    var xmlhttp = getXMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var xmlDoc = xmlhttp.responseXML;
            leschats = xmlDoc.getElementsByTagName("CAT");
            var table = "<tr><th>Nom</th><th>Pelage</th><th>Description</th></tr>";

            for (let i = 0; i < leschats.length; i++) {

                table += "<tr onclick='handleClick(" + leschats[i].getElementsByTagName("ID")[0].childNodes[0].nodeValue + ")' >";
                table += "<td>";
                table += leschats[i].getElementsByTagName("NOM")[0].childNodes[0].nodeValue;
                table += "</td><td>";
                table += leschats[i].getElementsByTagName("COULEUR")[0].childNodes[0].nodeValue;
                table += "</td><td>";
                table += leschats[i].getElementsByTagName("DESCRIPTION")[0].childNodes[0].nodeValue;
                table += "</td>";
                table += "</tr>";
            }
            document.getElementById("showCat").innerHTML = table;
        };
    }
    xmlhttp.open("GET", "source/cat_catalog.xml", true);
    xmlhttp.send();
}

function handleClick(id) {

    for (let i = 0; i < leschats.length; i++) {

        if (parseInt(leschats[i].getElementsByTagName("ID")[0].childNodes[0].nodeValue, 10) === parseInt(id, 10)) {
            
            const detail = leschats[i].getElementsByTagName("DETAIL")[0].childNodes[0].nodeValue;
            // Récupérer la div
            const tellMeMore = document.getElementById("tellMeMore");

            // Créer un paragraphe
            const paragraphe = document.createElement("p");

            // Ajouter du texte au paragraphe
            paragraphe.innerHTML = leschats[i].getElementsByTagName("NOM")[0].childNodes[0].nodeValue + " : " + detail;

            // Ajouter le paragraphe à la div
            tellMeMore.appendChild(paragraphe);
            return;
        }
    }
    console.error("Impossible de récupérer le DETAIL associé à l'id du chat");
    return;
}